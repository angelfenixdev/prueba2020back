
'use strict';

const bodyParser = require("body-parser");

var custom = require('./customEndpoints')


module.exports = function (server) {
  // Install a `/` route that returns server status
  server.use(bodyParser.urlencoded({ extended: false }));
  server.use(bodyParser.json());
  var router = server.loopback.Router();
  router.get('/', server.loopback.status());
  router.get('/getUsers', custom.getUsersArray)
  router.post('/downloadUserData', custom.downloadUserData)
  server.use(router);

  //1.
  Chofer.find({
    include: {
      relation: 'viajes',
      scope: {
        where: { fecha: { between: [firstDayLastMonth, lastDayLastMonth] } }
      }
    },
  }).subscribe((choferes: Chofer[]) => {
    choferes.sort(function (a, b) { return b.viajes.length - a.viajes.length });
    const choferMasViajes = choferes[0]
    console.log("El chofer con mas viajes es")
    console.log(JSON.stringify(choferMasViajes,null,2))
  })

  //2.


  Empresa.find({ include: 'clientes' }).subscribe((empresas) => {
    for (var empresa of empresas) {
      var clientesWithDistances = getDistances(empresa.latitud, empresa.longitud, empresa.clientes);
      clientesWithDistances.sort(function (a, b) { return b.distancia - a.distancia }).slice(0,4);
      console.log('Clientes mas lejanos de empresa '+ empresa.nombre)
      for(var cliente of clientesWithDistances)
      {
        console.log(JSON.stringify(cliente,null,2))
      }
      console.log("/////////////////////////////////")
    }
  })

  //Haciendolo de acuerdo a la documentacion de google maps distance matrix 
  // https://developers.google.com/maps/documentation/distance-matrix/overview
  getDistances(empresaLatitud, empresaLongitud, clientes)
  {
    var clientesCoordinates = clientes.map(c => { c.latitud, c.longitud })
    params = {
      origins: { empresaLatitud, empresaLongitud },
      destinations: clientesCoordinates
    }

    request('http://maps.googleapis.com/maps/api/distancematrix/json?parameters' + params, { json: true }, (err, res, comments) => {
      var distances = res.rows[0].elements.map(d => d.distance.value);
      for (var i = 0; i < distances.length; i++) {
        //add the property distancia to each client object so we can sort them later
        clientes[i].distancia = distances[0];
      }
      return clientes;

    })
  }
  //3.

  var clientes = Clientes.find({
    include: {
      relation: 'pedidos'
    },
  }).subscribe((clientes: Cliente[]) => {
    clientes.sort(function (a, b) { return b.pedidos.length - a.pedidos.length });
    var clientesFrecuentes = clientes.slice(0, 4)
    console.log("Clientes Frecuentes")
    for(var cliente of clientesFrecuentes)
    {
      console.log(JSON.stringify(cliente,null,2))
    }
    
  });



  //4.
  ultimosPedidos = Pedido.find({
    order: 'fecha DESC',
    limit: 10,
    include: ['cliente', {
      relation: 'viaje',
      scope: {
        include:
          ['chofer', 'camioneta']
      }
    }]
  }).subscribe((ultimosPedidos: Pedido[]) => {
    console.log("Ultimos 10 Pedidos")
    for (var pedido of ultimosPedidos) {
      console.log(pedido.idPedido + ' | ' + pedido.cliente.nombre + ' | ' + pedido.viaje.chofer.nombre
        + ' | ' + pedido.viaje.camioneta.matricula)
    }
  })


};
