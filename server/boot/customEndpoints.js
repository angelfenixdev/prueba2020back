
const request = require('request');
var fs = require('fs');

async function downloadUserData(request, response) {

    var jsonData = request.body.user;
    var fileName = '' + jsonData.id + ' - ' + jsonData.nombre + '.json'
  
    fs.writeFile(fileName, JSON.stringify(jsonData, null, 2), function (err) {
      if (err) throw err;
  
      fs.readFile(fileName, function (err, data) {
        if (err) throw err;
  
        response.set('Content-Type', 'application/json')
        response['Content-Disposition'] = 'attachment; filename=export.json'
        response.send(data);
      })
    });
  }
  
  
  
  async function getUsersArray(requestData, response, next) {
  
    console.log(JSON.stringify(requestData.headers)) //does this count as the bonus?
  
    var datetime = new Date()
  
    const names = [
      { id: 1, name: 'Leanne Graham' },
      { id: 2, name: 'Ervin Howell' },
      { id: 3, name: 'Clementine Bauch' },
      { id: 4, name: 'Patricia Lebsack' },
      { id: 5, name: 'Chelsey Dietrich' },
      { id: 6, name: 'Mrs. Dennis Schulist' },
      { id: 7, name: 'Kurtis Weissnat' },
      { id: 8, name: 'Nicholas Runolfsdottir V' },
      { id: 9, name: 'Glenna Reichert' },
      { id: 10, name: 'Clementina DuBuque' },]
  
    var data = {
      users: [],
      meta: datetime
    }
    var r = await request('http://jsonplaceholder.typicode.com/posts');
    request('http://jsonplaceholder.typicode.com/posts', { json: true }, (err, res, posts) => {
  
      if (err) { return console.log(err); }
  
      request('http://jsonplaceholder.typicode.com/comments', { json: true }, (err, res, comments) => {
  
        if (err) { return console.log(err); }
        //fetch data and then process it on getUsers
        getUsers(posts, comments, names).then((users) => {
          data.users = users
          response.send(data)
        })
      });
    });
  }
  /*
  
   Users deberá ser un array de objetos conteniendo los ids de los usuarios, su nombre,
   el número de posts y el número de comments que han recibido por todos los posts y los 
   posts del primer endpoint. A su vez, los posts deberán tener una nueva propiedad que serán los comments. 
  
   En el endpoint, vivirá una constante que son los nombres de los usuarios por ids.
   Estos serán mezclados con la respuesta de posts para arrojar el array completo al 
   invocar este endpoint. 
  */
  
  async function getUsers(posts, comments, names) {
    var users = await initUserArray(names)
  
    for (var post of posts) {
      var commentsArray = comments.filter(c => c.postId == post.id)
      post.comments = commentsArray
      //usar los ids de los posts para ir repartiendolos a los users indicados en lugar de hacer mas loops
      //aumentar contadores y agregar este mismo post
      users[post.userId - 1].posts_amount += 1
      users[post.userId - 1].comments_amount += commentsArray.length
      users[post.userId - 1].posts.push(post)
    }
    return users;
    // console.log(JSON.stringify(users[0].posts[0]))
  }
  
  //creando los esqueletos de los objetos usuario, para no tener problemas con declaraciones y acumuladores mas adelante
  async function initUserArray(names) {
    var tempUserArray = []
    for (var i = 0; i < names.length; i++) {
      tempUserArray.push({ id: names[i].id, nombre: names[i].name, posts_amount: 0, comments_amount: 0, posts: [] })
    }
    return tempUserArray
  }

  module.exports.downloadUserData =  downloadUserData;
  module.exports.getUsersArray =  getUsersArray;

  